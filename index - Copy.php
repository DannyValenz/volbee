<!doctype html>
<!--[if lt IE 7 ]><html id="ie6" class="ie ie-old ie-super-old"><![endif]-->
<!--[if IE 7 ]>   <html id="ie7" class="ie ie-old ie-super-old"><![endif]-->
<!--[if IE 8 ]>   <html id="ie8" class="ie ie-old ie-super-old"><![endif]-->
<!--[if IE 9 ]>   <html id="ie9" class="ie ie-old"><![endif]-->
<!--[if gt IE 9]><!--><html><!--<![endif]-->
<head>

    <!-- Meta -->
    <meta charset="utf-8">
    <title>Volbee</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="yes" name="apple-mobile-web-app-capable">
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">

    <!-- Favicons
    <link rel="shortcut icon" sizes="16x16 24x24 32x32 48x48 64x64" href="/wp-content/favicon.ico"> -->

    <!-- Styles -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" >
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <!--[if lt IE 9]>
        <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="style.css"><!-- Reference to your stylesheet -->

</head>
<body>
    <header>
        <div class="container">
            <div class="col-md-8 col-md-offset-2">
            <h1>
                <i class="fa fa-heart"></i>
                ScrollMagic Demos
                <i class="fa fa-magic"></i>
            </h1>
            <hr>
            <h2>Animations Binded to Scroll</h2>
            <p class="lead">The last example only triggers the animation at the specified Scene trigger point. ScrollMagic can though bind your animation to the scroll event. This acts as a rewind and fast-forward scrubber for your animation.</p>
            </div>
        </div>
        <i class="fa fa-angle-double-down"></i>
    </header>
    <main>
        <div class="container">
            <div class="row"><!-- where the animation starts -->
                
                <div class="col-md-6 col-md-offset-3">
                <div class="wrap" id="scene">  
                    <h2>Magic Happens Here</h2>
                    <div id="animation"><i class="fa fa-heart"></i></div>
                </div>
                </div>
                
            </div>
            <div class="row"><!-- where the animation starts -->
                
                <div class="col-md-6 col-md-offset-3">
                <div class="wrap" id="scene2">  
                    <h2>Magic Happens Here</h2>
                    <div id="animation2"><i class="fa fa-heart"></i></div>
                </div>
                </div>
                
            </div>
         </div>
    </main>


    <script src="js/TweenMax.min.js"></script>
    <script src="js/jquery-1.12.4.min.js"></script>
    <script src="js/ScrollMagic.js"></script>
    <script src="js/jquery.ScrollMagic.js"></script>
    <script src="js/animation.gsap.js"></script>
    <script src="js/debug.addIndicators.js"></script><!-- Dev only -->
    <script src="js/bootstrap.min.js"></script>

    <script type="text/javascript">        
        $(function() {
            
            // Init ScrollMagic Controller
            var scrollMagicController = new ScrollMagic.Controller();
            // Animation will be ignored and replaced by scene value in this example
            var tween = TweenMax.to('#animation', 0.5, {
                backgroundColor: 'rgb(255, 39, 46)',
                scale: 5,
                rotation: 360
            });
            var tween2 = TweenMax.to('#animation2', 0.5, {
                backgroundColor: 'rgb(255, 39, 46)',
                scale: 5,
                rotation: 360
            });
            
            // Create the Scene and trigger when visible
            var scene = new ScrollMagic.Scene({
                triggerElement: '#scene',
                triggerHook: 'onLeave',
                duration: 300 /* How many pixels to scroll / animate */
            })
            .setTween(tween)
            .addTo(scrollMagicController);
            
            // Add debug indicators fixed on right side
            scene.addIndicators();
            
            var scene2 = new ScrollMagic.Scene({
                triggerElement: '#scene2',
                triggerHook: 'onEnter',
                duration: 300 /* How many pixels to scroll / animate */
            })
            .setTween(tween2)
            .addTo(scrollMagicController);

            scene2.addIndicators();
        
        });
    </script>
</body>
</html>