<!doctype html>
<!--[if lt IE 7 ]><html id="ie6" class="ie ie-old ie-super-old"><![endif]-->
<!--[if IE 7 ]>   <html id="ie7" class="ie ie-old ie-super-old"><![endif]-->
<!--[if IE 8 ]>   <html id="ie8" class="ie ie-old ie-super-old"><![endif]-->
<!--[if IE 9 ]>   <html id="ie9" class="ie ie-old"><![endif]-->
<!--[if gt IE 9]><!--><html><!--<![endif]-->
<head>

    <!-- Meta -->
    <meta charset="utf-8">
    <title>Volbee</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">


    <!-- Favicons
    <link rel="shortcut icon" sizes="16x16 24x24 32x32 48x48 64x64" href="/wp-content/favicon.ico"> -->

    <!-- Styles -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="css/bootstrap.min.css" >
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <!--[if lt IE 9]>
        <script src="//oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="//oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <link rel="stylesheet" href="style.css"><!-- Reference to your stylesheet -->
</head>
<body>
    <main>
        <div class="container">
            <div class="fixed-image-wrapper" style="" >
                <img src="imgs/circle-battery.png" alt="Battery" style="position:absolute; opacity:0;" id="battery-image"/>
                <img src="imgs/slide-5-main-image.png" alt="Tesla"  style="position:absolute; opacity:0;"  id="tesla-image"/>
                <img src="imgs/slide-8-image.png" alt="Family 1"  style="position:absolute; opacity:0;"  id="family-image-1"/>
                <img src="imgs/main-volbee.png" alt="main-image" id="volbee-image" />
                
            </div>
            <div class="fixed-icon-wrapper">
                <a href="#Power"><i class="icon-icon-battery menu-icon" id="battery-icon"></i></a>
                <a href="#Technology"><i class="icon-icon-wave menu-icon" id="wave-icon"></i></a>
                <a href="#Cents"><i class="icon-icon-money menu-icon" id="money-icon"></i></a>
                <a href="#Alerts"><i class="icon-icon-alarm menu-icon" id="alarm-icon"></i></a>
                <a href="#Light"><i class="icon-icon-lightbulb menu-icon" id="lightbulb-icon"></i></a>
            </div>
            <div class="row no-gutter">
                <div class="col-xs-12 volbee-slide" style="" id="slide-00">
                    
                </div>
            </div>
            <div class="row no-gutter">
                <div class="col-xs-12 volbee-slide" style="" id="slide-01">
                    <img src="imgs/slide-2.png" alt="main-image"/>
                </div>
            </div>
            <div class="row no-gutter">
                <a id="Power"></a>
                <div class="col-xs-12 volbee-slide" style="" id="slide-02">
                    <img src="imgs/slide-3.png" alt="main-image"/>
                </div>
            </div>
            <div class="row no-gutter">
                <div class="col-xs-12 volbee-slide" style="" id="slide-03">
                    <img src="imgs/slide-4.png" alt="main-image"/>
                </div>
            </div>
            <div class="row no-gutter">
                <a id="Technology"></a>
                <div class="col-xs-12 volbee-slide" style="" id="slide-04">
                    <img src="imgs/slide-5.png" alt="main-image"/>
                </div>
            </div>
            <div class="row no-gutter">
                <a id="Cents"></a>
                <div class="col-xs-12 volbee-slide" style="" id="slide-05">             
                </div>
            </div>
            <div class="row no-gutter">
                <a id="Alerts"></a>
                <div class="col-xs-12 volbee-slide" style="" id="slide-06">
                    <div class="growing-circles"></div>
                    <img src="imgs/slide-7.png" alt="main-image"/>
                </div>
            </div>
            <div class="row no-gutter">
                <div class="col-xs-12 volbee-slide" style="" id="slide-07">
             
                </div>
            </div>
            <div class="row no-gutter">
                <a id="Light"></a>
                <div class="col-xs-12 volbee-slide" style="" id="slide-08">
                    <div class="growing-mask" ></div>
                    <img src="imgs/slide-9.png" alt="main-image"/>             
                </div>
            </div>
        </div>
    </main>


    <script src="js/TweenMax.min.js"></script>
    <script src="js/jquery-1.12.4.min.js"></script>
    <script src="js/ScrollMagic.js"></script>
    <script src="js/jquery.ScrollMagic.js"></script>
    <script src="js/animation.gsap.js"></script>
    <script src="js/ScrollToPlugin.min.js"></script>
    <script src="js/EaselPlugin.min.js"></script>
    <script src="js/debug.addIndicators.js"></script><!-- Dev only -->
    <script src="js/bootstrap.min.js"></script>
    <script src="js/script.js"></script>

</body>
</html>