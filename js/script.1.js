$(function() {
    
    // Init ScrollMagic Controller
    var scrollMagicController = new ScrollMagic.Controller();
    // Create the Scene and trigger when visible


    var menuSlideinTween = TweenMax.staggerTo(".menu-icon", 0.5, {opacity: 1, left:0},0.1 /* Stagger duration */);



    var slide0 = new ScrollMagic.Scene({
        triggerElement: '#slide-00',
        triggerHook: 'onLeave', 
        offset: 1,
        reverse:false,
        duration: 150
        //, duration: 300 //makes animation work via frames
    })
    .setTween(menuSlideinTween)
    .addTo(scrollMagicController);
    //slide0.addIndicators();

    //var menuColorChangeTween = TweenMax.staggerTo(".menu-icon", 0.5, {opacity: 1, left:0},0.1 /* Stagger duration */);
    
    var lightBulbSlide00to01 = new ScrollMagic.Scene({
        triggerElement: '#slide-01',
        offset: -150,
        duration: 200
    })
    .setTween("#lightbulb-icon",1 , {color: '#ffffff',opacity: 1})
    .addTo(scrollMagicController);
    //lightBulbSlide00to01.addIndicators();
    
    var alarmSlide00to01 = new ScrollMagic.Scene({
        triggerElement: '#slide-01',
        offset: -60,
        duration: 200
    })
    .setTween("#alarm-icon",1 , {color: '#ffffff',opacity: 1})
    .addTo(scrollMagicController);
    //alarmSlide00to01.addIndicators();
    
    var moneySlide00to01 = new ScrollMagic.Scene({
        triggerElement: '#slide-01',
        offset: 30,
        duration: 200
    })
    .setTween("#money-icon",1 , {color: '#ffffff',opacity: 1})
    .addTo(scrollMagicController);
    //moneySlide00to01.addIndicators();
    
    var waveSlide00to01 = new ScrollMagic.Scene({
        triggerElement: '#slide-01',
        offset: 120,
        duration: 200
    })
    .setTween("#wave-icon",1 , {color: '#ffffff',opacity: 1})
    .addTo(scrollMagicController);
    //waveSlide00to01.addIndicators();
    
    var batterySlide00to01 = new ScrollMagic.Scene({
        triggerElement: '#slide-01',
        offset: 210,
        duration: 200
    })
    .setTween("#battery-icon",1 , {color: '#ffffff',opacity: 1})
    .addTo(scrollMagicController);
    //batterySlide00to01.addIndicators();


    var slide2ImageTransitions = new TimelineMax()
    .to("#volbee-image", 1,  {opacity: 0})
    .to("#battery-image", 1, {opacity: 1}, 0);

    var slide2 = new ScrollMagic.Scene({
        triggerElement: '#slide-02',
        duration: 300
    })
    .setTween(slide2ImageTransitions)
    .addTo(scrollMagicController);
    //slide2.addIndicators();

    var slide3ImageTransitions = new TimelineMax()
    .to("#volbee-image", 1,  {opacity: 1})
    .to("#battery-image", 1, {opacity: 0}, 0);

    var slide3 = new ScrollMagic.Scene({
        triggerElement: '#slide-03',
        duration: 300
        //, duration: 300 //makes animation work via frames
    })
    .setTween(slide3ImageTransitions)
    .addTo(scrollMagicController);
    //slide3.addIndicators();





    var lightBulbSlide01to02 = new ScrollMagic.Scene({
        triggerElement: '#slide-02',
        offset: -150,
        duration: 200
    })
    .setTween("#lightbulb-icon",1 ,{opacity: .25})
    .addTo(scrollMagicController);
    //lightBulbSlide01to02.addIndicators();

    var alarmSlide01to02 = new ScrollMagic.Scene({
        triggerElement: '#slide-02',
        offset: -60,
        duration: 200
    })
    .setTween("#alarm-icon",1 ,{opacity: .25})
    .addTo(scrollMagicController);
    //alarmSlide01to02.addIndicators();

    var moneySlide01to02 = new ScrollMagic.Scene({
        triggerElement: '#slide-02',
        offset: 30,
        duration: 200
    })
    .setTween("#money-icon",1 ,{opacity: .25})
    .addTo(scrollMagicController);
    //moneySlide01to02.addIndicators();

    var waveSlide01to02 = new ScrollMagic.Scene({
        triggerElement: '#slide-02',
        offset: 120,
        duration: 200
    })
    .setTween("#wave-icon",1 ,{opacity: .25})
    .addTo(scrollMagicController);
    //waveSlide01to02.addIndicators();
   
    //////////////////////////////////////////////////

    var lightBulbSlide02to03 = new ScrollMagic.Scene({
        triggerElement: '#slide-03',
        offset: -150,
        duration: 200
    })
    .setTween("#lightbulb-icon",1 ,{opacity: 1})
    .addTo(scrollMagicController);
    //lightBulbSlide01to02.addIndicators();

    var alarmSlide02to03 = new ScrollMagic.Scene({
        triggerElement: '#slide-03',
        offset: -60,
        duration: 200
    })
    .setTween("#alarm-icon",1 ,{opacity: 1})
    .addTo(scrollMagicController);
    //alarmSlide01to02.addIndicators();

    var moneySlide02to03 = new ScrollMagic.Scene({
        triggerElement: '#slide-03',
        offset: 30,
        duration: 200
    })
    .setTween("#money-icon",1 ,{opacity: 1})
    .addTo(scrollMagicController);
    //moneySlide01to02.addIndicators();

    var waveSlide02to03 = new ScrollMagic.Scene({
        triggerElement: '#slide-03',
        offset: 120,
        duration: 200
    })
    .setTween("#wave-icon",1 ,{opacity: 1})
    .addTo(scrollMagicController);
   
    //////////////////////////////////////////////////

    var lightBulbSlide03to04 = new ScrollMagic.Scene({
        triggerElement: '#slide-04',
        offset: -150,
        duration: 200
    })
    .setTween("#lightbulb-icon",1 ,{opacity: .25 })
    .addTo(scrollMagicController);

    var alarmSlide03to04 = new ScrollMagic.Scene({
        triggerElement: '#slide-04',
        offset: -60,
        duration: 200
    })
    .setTween("#alarm-icon",1 ,{opacity: .25 })
    .addTo(scrollMagicController);

    var moneySlide03to04 = new ScrollMagic.Scene({
        triggerElement: '#slide-04',
        offset: 30,
        duration: 200
    })
    .setTween("#money-icon",1 ,{opacity: .25 })
    .addTo(scrollMagicController);    
    
    var batterySlide03to04 = new ScrollMagic.Scene({
        triggerElement: '#slide-04',
        offset: 210,
        duration: 200
    })
    .setTween("#battery-icon",1 , {opacity: .25 })
    .addTo(scrollMagicController);
   
    //////////////////////////////////////////////////
    
    var lightBulbSlide04to05 = new ScrollMagic.Scene({
        triggerElement: '#slide-05',
        offset: -150,
        duration: 200
    })
    .setTween("#lightbulb-icon",1 ,{opacity: 1, color:"#D1D1D3"})
    .addTo(scrollMagicController);

    var alarmSlide04to05 = new ScrollMagic.Scene({
        triggerElement: '#slide-05',
        offset: -60,
        duration: 200
    })
    .setTween("#alarm-icon",1 ,{opacity: 1, color:"#D1D1D3"})
    .addTo(scrollMagicController);

    var moneySlide04to05 = new ScrollMagic.Scene({
        triggerElement: '#slide-05',
        offset: 30,
        duration: 200
    })
    .setTween("#money-icon",1 ,{opacity: 1, color:"#238FBD"})
    .addTo(scrollMagicController);   

    var waveSlide04to05 = new ScrollMagic.Scene({
        triggerElement: '#slide-05',
        offset: 120,
        duration: 200
    })
    .setTween("#wave-icon",1 ,{opacity: 1, color:"#D1D1D3"})
    .addTo(scrollMagicController); 
    
    var batterySlide04to05 = new ScrollMagic.Scene({
        triggerElement: '#slide-05',
        offset: 210,
        duration: 200
    })
    .setTween("#battery-icon",1 , {opacity: 1, color:"#D1D1D3"})
    .addTo(scrollMagicController);
    
    //////////////////////////////////////////////////
    
    var lightBulbSlide05to06 = new ScrollMagic.Scene({
        triggerElement: '#slide-06',
        offset: -150,
        duration: 200
    })
    .setTween("#lightbulb-icon",1 ,{opacity: .25, color:"#FFFFFF"})
    .addTo(scrollMagicController);

    var alarmSlide05to06 = new ScrollMagic.Scene({
        triggerElement: '#slide-06',
        offset: -60,
        duration: 200
    })
    .setTween("#alarm-icon",1 ,{opacity: 1, color:"#FFFFFF"})
    .addTo(scrollMagicController);

    var moneySlide05to06 = new ScrollMagic.Scene({
        triggerElement: '#slide-06',
        offset: 30,
        duration: 200
    })
    .setTween("#money-icon",1 ,{opacity: .25, color:"#FFFFFF"})
    .addTo(scrollMagicController);   

    var waveSlide05to06 = new ScrollMagic.Scene({
        triggerElement: '#slide-06',
        offset: 120,
        duration: 200
    })
    .setTween("#wave-icon",1 ,{opacity: .25, color:"#FFFFFF"})
    .addTo(scrollMagicController); 
    
    var batterySlide05to06 = new ScrollMagic.Scene({
        triggerElement: '#slide-06',
        offset: 210,
        duration: 200
    })
    .setTween("#battery-icon",1 , {opacity: .25, color:"#FFFFFF"})
    .addTo(scrollMagicController);
    
    //////////////////////////////////////////////////
    
    var lightBulbSlide06to07 = new ScrollMagic.Scene({
        triggerElement: '#slide-07',
        offset: -150,
        duration: 200
    })
    .setTween("#lightbulb-icon",1 ,{opacity: 1, color:"#D1D1D3"})
    .addTo(scrollMagicController);

    var alarmSlide06to07 = new ScrollMagic.Scene({
        triggerElement: '#slide-07',
        offset: -60,
        duration: 200
    })
    .setTween("#alarm-icon",1 ,{opacity: 1, color:"#D1D1D3"})
    .addTo(scrollMagicController);

    var moneySlide06to07 = new ScrollMagic.Scene({
        triggerElement: '#slide-07',
        offset: 30,
        duration: 200
    })
    .setTween("#money-icon",1 ,{opacity: 1, color:"#D1D1D3"})
    .addTo(scrollMagicController);   

    var waveSlide06to07 = new ScrollMagic.Scene({
        triggerElement: '#slide-07',
        offset: 120,
        duration: 200
    })
    .setTween("#wave-icon",1 ,{opacity: 1, color:"#D1D1D3"})
    .addTo(scrollMagicController); 
    
    var batterySlide06to07 = new ScrollMagic.Scene({
        triggerElement: '#slide-07',
        offset: 210,
        duration: 200
    })
    .setTween("#battery-icon",1 , {opacity: 1, color:"#D1D1D3"})
    .addTo(scrollMagicController);
    
    //////////////////////////////////////////////////
    
    var lightBulbSlide07to08 = new ScrollMagic.Scene({
        triggerElement: '#slide-08',
        offset: -150,
        duration: 200
    })
    .setTween("#lightbulb-icon",1 ,{opacity: 1, color:"#FFFFFF"})
    .addTo(scrollMagicController);

    var alarmSlide07to08 = new ScrollMagic.Scene({
        triggerElement: '#slide-08',
        offset: -60,
        duration: 200
    })
    .setTween("#alarm-icon",1 ,{opacity: .25, color:"#FFFFFF"})
    .addTo(scrollMagicController);

    var moneySlide07to08 = new ScrollMagic.Scene({
        triggerElement: '#slide-08',
        offset: 30,
        duration: 200
    })
    .setTween("#money-icon",1 ,{opacity: .25, color:"#FFFFFF"})
    .addTo(scrollMagicController);   

    var waveSlide07to08 = new ScrollMagic.Scene({
        triggerElement: '#slide-08',
        offset: 120,
        duration: 200
    })
    .setTween("#wave-icon",1 ,{opacity: .25, color:"#FFFFFF"})
    .addTo(scrollMagicController); 
    
    var batterySlide07to08 = new ScrollMagic.Scene({
        triggerElement: '#slide-08',
        offset: 210,
        duration: 200
    })
    .setTween("#battery-icon",1 , {opacity: .25, color:"#FFFFFF"})
    .addTo(scrollMagicController);
    



    var slide4ImageTransitions = new TimelineMax()
    .to("#volbee-image", 1,  {opacity: 0})
    .to("#tesla-image", 1, {opacity: 1}, 0);

    var waveSlide04to05 = new ScrollMagic.Scene({
        triggerElement: '#slide-04',
        offset: 120,
        duration: 200
    })
    .setTween(slide4ImageTransitions)
    .addTo(scrollMagicController);
    

    var slide5ImageTransitions = new TimelineMax()
    .to("#volbee-image", 1,  {opacity: 1})
    .to("#tesla-image", 1, {opacity: 0}, 0);

    var waveSlide05to06 = new ScrollMagic.Scene({
        triggerElement: '#slide-05',
        offset: 120,
        duration: 200
    })
    .setTween(slide5ImageTransitions)
    .addTo(scrollMagicController);

    /////////////////////////////////////////////


    var slide6to07 = new ScrollMagic.Scene({
        triggerElement: '#slide-06',
        triggerHook: 'onLeave',
        //reverse: false, 
        offset: -20,
        //duration: 200
    })
    .setTween(".growing-circles", 1.5, { backgroundSize:"1813px 1789px" ,'background-position-x': '-630px', 'background-position-y': '-537px'})
    .addTo(scrollMagicController)
    //.addIndicators();
    

    var slide7ImageTransitions = new TimelineMax()
    .to("#volbee-image", 1,  {opacity: 0})
    .to("#family-image-1", 1, {opacity: 1}, 0);

    var slide07to08 = new ScrollMagic.Scene({
        triggerElement: '#slide-07',
        offset: 120,
        duration: 200
    })
    .setTween(slide7ImageTransitions)
    .addTo(scrollMagicController);
    

    var slide8ImageTransitions = new TimelineMax()
    .to("#volbee-image", 1,  {opacity: 1})
    .to("#family-image-1", 1, {opacity: 0}, 0);

    var slide08to09 = new ScrollMagic.Scene({
        triggerElement: '#slide-08',
        offset: 120,
        duration: 200
    })
    .setTween(slide8ImageTransitions)
    .addTo(scrollMagicController);

    var slide8to09 = new ScrollMagic.Scene({
        triggerElement: '#slide-08',
        triggerHook: 'onLeave',
        //reverse: false, 
        offset: -20,
        //duration: 200
    })//-500px -770px
    .setTween(".growing-mask", 1.5, { backgroundSize:"3600px 2454px",'background-position-x': '-500px', 'background-position-y': '-770px', backgroundColor:'none'})
    .addTo(scrollMagicController)
    //.addIndicators();
    

    /*
    slide2.on("start", function (event) {
        //alert("Hit start point of scene.");
    });
    */
    $(window).scroll(function(){
        $('.fixed-image-wrapper').css('margin-left',-$(window).scrollLeft());
    });

	// change behaviour of controller to animate scroll instead of jump
	scrollMagicController.scrollTo(function (newpos) {
            console.log(newpos);
		TweenMax.to(window, 0.5, {scrollTo: {y: newpos}});
	});


    $(document).on("click", "a[href^='#']", function (e) {
		var id = $(this).attr("href");
		if ($(id).length > 0) {
			e.preventDefault();

			// trigger scroll
			scrollMagicController.scrollTo(id);

            // if supported by the browser we can even update the URL.
			if (window.history && window.history.pushState) {
				history.pushState("", document.title, id);
			}
		}
	});
});