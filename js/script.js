$(function() {
    
    // Init ScrollMagic Controller
    var scrollMagicController = new ScrollMagic.Controller();
    // Create the Scene and trigger when visible
    var lastScrollTop = window.pageYOffset || document.documentElement.scrollTop;
    var lastScrollDirection = null;

    var menuSlideinTween = TweenMax.staggerTo(".menu-icon", 0.5, {opacity: 1, left:0},0.1 /* Stagger duration */);

    /////////////////////////////////////////
    //  SLIDE 01
    /////////////////////////////////////////
    var slide1 = new ScrollMagic.Scene({
        triggerElement: '#slide-01',
        triggerHook: 'onLeave', 
        offset: 1,
        reverse:false,
        duration: vhToPixels(15)
        //, duration: 300 //makes animation work via frames
    })
    .setTween(menuSlideinTween)
    .addTo(scrollMagicController);


    /////////////////////////////////////////
    //  SLIDE 02
    /////////////////////////////////////////
    SetMenuTween('#slide-02', vhToPixels(-15), vhToPixels(20), "#lightbulb-icon", {color: '#ffffff',opacity: 1});
    SetMenuTween('#slide-02', vhToPixels(-6), vhToPixels(20), "#alarm-icon", {color: '#ffffff',opacity: 1});
    SetMenuTween('#slide-02', vhToPixels(3), vhToPixels(20), "#money-icon", {color: '#ffffff',opacity: 1});
    SetMenuTween('#slide-02', vhToPixels(12), vhToPixels(20), "#wave-icon", {color: '#ffffff',opacity: 1});
    SetMenuTween('#slide-02', vhToPixels(21), vhToPixels(15), "#battery-icon", {color: '#ffffff',opacity: 1});      


    /////////////////////////////////////////
    //  SLIDE 03
    /////////////////////////////////////////
    SetMenuTween('#slide-03', vhToPixels(-15), vhToPixels(20), "#lightbulb-icon", {opacity: .25});
    SetMenuTween('#slide-03', vhToPixels(-6), vhToPixels(20), "#alarm-icon", {opacity: .25});
    SetMenuTween('#slide-03', vhToPixels(3), vhToPixels(20), "#money-icon", {opacity: .25});
    SetMenuTween('#slide-03', vhToPixels(12), vhToPixels(20), "#wave-icon", {opacity: .25});
    //SetMenuTween('#slide-03', vhToPixels(21), vhToPixels(15), "#battery-icon", {opacity: .25});    

    /*-------------------Lightening layer 1, settup ------------------------------------*/
    var lighteningLayer1 = new TimelineMax()
        .to("#slide3-background-layer1", 2, {top: 0}); //the div is settet to -100vh, so 0vh means it's going to start falling
    var lighteningLayer1out = new TimelineMax()
        .to("#slide3-background-layer1", 2, {top: '200vh'}); // with the 0vh assumption due lighteningLayer1

        //when scroll is at 1 point before 50% lightings layer 1 starts appearing down
    var slide3LighteningLayer1 = new ScrollMagic.Scene({
            triggerElement: '#slide-03',
            offset: vhToPixels(40)
        })
        .setTween(lighteningLayer1)
        .addTo(scrollMagicController)
        //.addIndicators();

        //when scroll is at 1 point after 50% lightings layer 1 starts appearing down
    var slide3LighteningLayer1out = new ScrollMagic.Scene({
            triggerElement: '#slide-03',
            offset: vhToPixels(60)
        })
        .setTween(lighteningLayer1out)
        .addTo(scrollMagicController)
        //.addIndicators();
    /*----------------------------------------------------------------------------------*/

    /*-------------------Lightening layer 2, settup ------------------------------------*/
    var lighteningLayer2 = new TimelineMax()
        .to("#slide3-background-layer2", 4, {top: 0}); //the div is settet to -100vh, so 0vh means it's going to start falling
    var lighteningLayer2out = new TimelineMax()
        .to("#slide3-background-layer2", 3, {top: '200vh'}); // with the 0vh assumption due lighteningLayer1

    var slide3LighteningLayer2 = new ScrollMagic.Scene({
            triggerElement: '#slide-03',
            offset: vhToPixels(40)
        })
        .setTween(lighteningLayer2)
        .addTo(scrollMagicController)
        //.addIndicators();
    var slide3LighteningLayer2 = new ScrollMagic.Scene({
            triggerElement: '#slide-03',
            offset: vhToPixels(60)
        })
        .setTween(lighteningLayer2out)
        .addTo(scrollMagicController)
        //.addIndicators();
    /*----------------------------------------------------------------------------------*/

    var slide2ImageTransitions = new TimelineMax()
    .to("#volbee-image", 1,  {opacity: 0})
    .to("#battery-image", 1, {opacity: 1}, 0);

    var slide2 = new ScrollMagic.Scene({
        triggerElement: '#slide-03',
        duration: vhToPixels(45)
    })
    .setTween(slide2ImageTransitions)
    .addTo(scrollMagicController);
    //slide2.addIndicators();

    /////////////////////////////////////////
    //  SLIDE 04
    /////////////////////////////////////////  
    SetMenuTween('#slide-04', vhToPixels(-15), vhToPixels(20), "#lightbulb-icon", {opacity: 1});
    SetMenuTween('#slide-04', vhToPixels(-6), vhToPixels(20), "#alarm-icon", {opacity: 1});
    SetMenuTween('#slide-04', vhToPixels(3), vhToPixels(20), "#money-icon", {opacity: 1});
    SetMenuTween('#slide-04', vhToPixels(12), vhToPixels(20), "#wave-icon", {opacity: 1});
    //SetMenuTween('#slide-04', vhToPixels(21), vhToPixels(15), "#battery-icon", {opacity: 1});  


    var slide4ImageTransitions = new TimelineMax()
    .to("#volbee-image", 1,  {opacity: 1})
    .to("#battery-image", 1, {opacity: 0}, 0);

    var slide4 = new ScrollMagic.Scene({
        triggerElement: '#slide-04',
        duration: vhToPixels(45)
    })
    .setTween(slide4ImageTransitions)
    .addTo(scrollMagicController);

    /////////////////////////////////////////
    //  SLIDE 05
    /////////////////////////////////////////  
    SetMenuTween('#slide-05', vhToPixels(-15), vhToPixels(20), "#lightbulb-icon", {opacity: .25});
    SetMenuTween('#slide-05', vhToPixels(-6), vhToPixels(20), "#alarm-icon", {opacity: .25});
    SetMenuTween('#slide-05', vhToPixels(3), vhToPixels(20), "#money-icon", {opacity: .25});
    //SetMenuTween('#slide-05', vhToPixels(12), vhToPixels(20), "#wave-icon", {opacity: .25});
    SetMenuTween('#slide-05', vhToPixels(21), vhToPixels(15), "#battery-icon", {opacity: .25}); 

    var slide5ImageTransitions = new TimelineMax()
    .to("#volbee-image", 1,  {opacity: 0})
    .to("#tesla-image", 1, {opacity: 1}, 0);

    var waveSlide04to05 = new ScrollMagic.Scene({
        triggerElement: '#slide-05',
        offset: vhToPixels(12),
        duration: vhToPixels(45)
    })
    .setTween(slide5ImageTransitions)
    .addTo(scrollMagicController);

    /*-------------------Curves lines layer 1, settup ------------------------------------*/
    var linesLayer1In = new TimelineMax()
        .to('#lines-layer-1', 2, {left: 0})
        .to('#curves-panel-1', 4, {left: '100vw'});
    var linesLayer1Out = new TimelineMax()
        .to('#curves-panel-1', 4, {left: 0})
        .to('#lines-layer-1', 2, {left: '-100vw'});

    var slideLinesLayer1In = new ScrollMagic.Scene({
            triggerElement: '#slide-05',
            offset: vhToPixels(40)
        })
        .setTween(linesLayer1In)
        .addTo(scrollMagicController);
    var slideLinesLayer1Out = new ScrollMagic.Scene({
            triggerElement: '#slide-05',
            offset: vhToPixels(60)
        })
        .setTween(linesLayer1Out)
        .addTo(scrollMagicController);
    /*----------------------------------------------------------------------------------------*/

    /*-------------------Curves lines layer 2, settup ------------------------------------*/
    var linesLayer2In = new TimelineMax()
        .to('#lines-layer-2', 3, {left: 0})
        .to('#curves-panel-2', 4, {left: '100vw'})
    var linesLayer2Out = new TimelineMax()
        .to('#curves-panel-2', 4, {left: 0})
        .to('#lines-layer-2', 3, {left: '-100vw'});

    var slideLinesLayer2In = new ScrollMagic.Scene({
            triggerElement: '#slide-05',
            offset: vhToPixels(40)
        })
        .setTween(linesLayer2In)
        .addTo(scrollMagicController);
    var slideLinesLayer2Out = new ScrollMagic.Scene({
            triggerElement: '#slide-05',
            offset: vhToPixels(60)
        })
        .setTween(linesLayer2Out)
        .addTo(scrollMagicController);
    /*----------------------------------------------------------------------------------------*/

    

    /////////////////////////////////////////
    //  SLIDE 06
    /////////////////////////////////////////  
    SetMenuTween('#slide-06', vhToPixels(-15), vhToPixels(20), "#lightbulb-icon", {opacity: 1, color:"#D1D1D3"});
    SetMenuTween('#slide-06', vhToPixels(-6), vhToPixels(20), "#alarm-icon", {opacity: 1, color:"#D1D1D3"});
    SetMenuTween('#slide-06', vhToPixels(3), vhToPixels(20), "#money-icon", {opacity: 1, color:"#238FBD"});
    SetMenuTween('#slide-06', vhToPixels(12), vhToPixels(20), "#wave-icon", {opacity: 1, color:"#D1D1D3"});
    SetMenuTween('#slide-06', vhToPixels(21), vhToPixels(15), "#battery-icon", {opacity: 1, color:"#D1D1D3"});

    var slide6ImageTransitions = new TimelineMax()
    .to("#volbee-image", 1,  {opacity: 1})
    .to("#tesla-image", 1, {opacity: 0}, 0);

    var waveSlide05to06 = new ScrollMagic.Scene({
        triggerElement: '#slide-06',
        offset: vhToPixels(12),
        duration: vhToPixels(45)
    })
    .setTween(slide6ImageTransitions)
    .addTo(scrollMagicController);

    /////////////////////////////////////////
    //  SLIDE 07
    /////////////////////////////////////////  
    SetMenuTween('#slide-07', vhToPixels(-15), vhToPixels(20), "#lightbulb-icon", {opacity: .25, color:"#FFFFFF"});
    SetMenuTween('#slide-07', vhToPixels(-6), vhToPixels(20), "#alarm-icon", {opacity: 1, color:"#FFFFFF"});
    SetMenuTween('#slide-07', vhToPixels(3), vhToPixels(20), "#money-icon", {opacity: .25, color:"#FFFFFF"});
    SetMenuTween('#slide-07', vhToPixels(12), vhToPixels(20), "#wave-icon", {opacity: .25, color:"#FFFFFF"});
    SetMenuTween('#slide-07', vhToPixels(21), vhToPixels(15), "#battery-icon", {opacity: .25, color:"#FFFFFF"}); 



    /*-------------------Curves lines layer 2, settup ------------------------------------*/
    var growingCirclesTransitions = new TimelineMax()
    .to('#growing-circles', 3, {
        scale: 2
    });

    var slide07GrowingCircles = new ScrollMagic.Scene({
        triggerElement: '#slide-07',
        offset: vhToPixels(49),
    })
    .setTween(growingCirclesTransitions)
    .addTo(scrollMagicController).addIndicators();
    /*----------------------------------------------------------------------------------------*/


    /////////////////////////////////////////
    //  SLIDE 08
    /////////////////////////////////////////  
    SetMenuTween('#slide-08', vhToPixels(-15), vhToPixels(20), "#lightbulb-icon", {opacity: 1, color:"#D1D1D3"});
    SetMenuTween('#slide-08', vhToPixels(-6), vhToPixels(20), "#alarm-icon", {opacity: 1, color:"#D1D1D3"});
    SetMenuTween('#slide-08', vhToPixels(3), vhToPixels(20), "#money-icon", {opacity: 1, color:"#D1D1D3"});
    SetMenuTween('#slide-08', vhToPixels(12), vhToPixels(20), "#wave-icon", {opacity: 1, color:"#D1D1D3"});
    SetMenuTween('#slide-08', vhToPixels(21), vhToPixels(15), "#battery-icon", {opacity: 1, color:"#D1D1D3"});

    var slide8ImageTransitions = new TimelineMax()
    .to("#volbee-image", 1,  {opacity: 0})
    .to("#family-image-1", 1, {opacity: 1}, 0);

    var slide07to08 = new ScrollMagic.Scene({
        triggerElement: '#slide-08',
        offset: vhToPixels(12),
        duration: vhToPixels(45)
    })
    .setTween(slide8ImageTransitions)
    .addTo(scrollMagicController);
    




    /////////////////////////////////////////
    //  SLIDE 09
    /////////////////////////////////////////  
    SetMenuTween('#slide-09', vhToPixels(-15), vhToPixels(20), "#lightbulb-icon", {opacity: 1, color:"#FFFFFF"});
    SetMenuTween('#slide-09', vhToPixels(-6), vhToPixels(20), "#alarm-icon", {opacity: .25, color:"#FFFFFF"});
    SetMenuTween('#slide-09', vhToPixels(3), vhToPixels(20), "#money-icon", {opacity: .25, color:"#FFFFFF"});
    SetMenuTween('#slide-09', vhToPixels(12), vhToPixels(20), "#wave-icon", {opacity: .25, color:"#FFFFFF"});
    SetMenuTween('#slide-09', vhToPixels(21), vhToPixels(15), "#battery-icon", {opacity: .25, color:"#FFFFFF"}); 

     var slide9ImageTransitions = new TimelineMax()
    .to("#volbee-image", 1,  {opacity: 1})
    .to("#family-image-1", 1, {opacity: 0}, 0);

    var slide08to09 = new ScrollMagic.Scene({
        triggerElement: '#slide-09',
        offset: vhToPixels(12),
        duration: vhToPixels(45)
    })
    .setTween(slide9ImageTransitions)
    .addTo(scrollMagicController);





    /////////////////////////////////////////
    //  CUSTOM FUNCTIONS
    /////////////////////////////////////////

    function SetMenuTween(target,offset,duration,icon,css)
    {
        var temp = new ScrollMagic.Scene({
            triggerElement: target,
            offset: offset,
            duration: duration
        })
        .setTween(icon, 1, css)
        .addTo(scrollMagicController);
        //temp.addIndicators();
    }

    function vhToPixels(vh)
    {
        var temp = Math.floor( $(window).height() * (vh/100) );
        //console.log(temp)
        return temp;
    }


    $(window).scroll(function(){
        //$('.fixed-image-wrapper').css('margin-left',-$(window).scrollLeft());

        var st = window.pageYOffset || document.documentElement.scrollTop; 
        if (st > lastScrollTop){
            lastScrollDirection = "down";
        } else {
            lastScrollDirection = "up";
        }
        lastScrollTop = st; 
            
        clearTimeout( $.data( this, "scrollCheck" ) );
        if(!TweenMax.isTweening(window)) 
        {          
            $.data(this, "scrollCheck", setTimeout(function() {           
                var halfWidth = Math.floor(document.documentElement.clientWidth/2) ;
                var slide = $(document.elementFromPoint(halfWidth,0));     
                if(!slide.hasClass('volbee-slide'))
                {
                    slide = slide.closest('.volbee-slide');
                }

                if(slide.offset().top!=st)
                {
                    if (lastScrollDirection == "down")
                    {                    
                        var nextSlide = slide.closest('.row').next().find('.volbee-slide');
                        var offset = nextSlide.css('margin-top').replace(/[^-\d\.]/g, '');
                        scrollMagicController.scrollTo(nextSlide.offset().top-offset);
                    }   
                    else
                    {
                        var offset = slide.css('margin-top').replace(/[^-\d\.]/g, '');
                        scrollMagicController.scrollTo(slide.offset().top-offset);
                    } 
                }            
            }, 400) );
        }
    });

	// change behaviour of controller to animate scroll instead of jump
	scrollMagicController.scrollTo(function (newpos) {
		TweenMax.to(window, 1, {scrollTo: {y: newpos, autoKill:false}});
	});


    $(document).on("click", "a[href^='#']", function (e) {
		var id = $(this).attr("href");
		if ($(id).length > 0) {
			e.preventDefault();

			// trigger scroll
			scrollMagicController.scrollTo(id);

            // if supported by the browser we can even update the URL.
			if (window.history && window.history.pushState) {
				history.pushState("", document.title, id);
			}
		}
	});
});